(function ($, Drupal) {
    'use strict';

    $(window).on("load", function() {
        //Homepage graphics being adjusted
        $(".graph-homepage-container:not(.is__static) .stripe_text").width(($(".homepage-bar").width()*3) + (parseInt($(".homepage-bar").css('margin-right'))*2)  + 90);
        $(".graph-homepage-line").width(($(".homepage-bar").width()*3) + (parseInt($(".homepage-bar").css('margin-right'))*2)  + 98);
        $(".graph-homepage-container:not(.is__static) .first_percentage").width(($(".homepage-bar").width()*3) + (parseInt($(".homepage-bar").css('margin-right'))*2)  + 68);
        $(".second_percentage").width(($(".homepage-bar").width()*2) + (parseInt($(".homepage-bar").css('margin-right')))  + 68);
        $(".third_percentage").width(($(".homepage-bar").width())  + 68);

        setTimeout(function(){
            $("#donate-container").css({
                "visibility" : "visible",
                "display" : "none"
            });
        }, 200);


        $("#newsletter-submit").on("click", function(){
            $.ajax({
                url: '/p/newsletter',
                data : {"email" : $("#form__newsletter #mailchimp-email").val()},
                method: 'POST',
                success: function(data) {

                    var response = JSON.parse(data);
                    var email = $("#form__newsletter #mailchimp-email").val();

                    if(response.status == 400)
                    {
                        if(response.title == "Invalid Resource")
                        {
                            if(response.errors)
                            {
                                $("#newsletter_response").removeClass("is_success").addClass("is_error").text(response.errors[0].message);
                            }
                            else
                            {
                                $("#newsletter_response").removeClass("is_success").addClass("is_error").text(response.detail);
                            }
                        }

                        if(response.title == "Member Exists")
                        {
                            $("#newsletter_response").removeClass("is_success").addClass("is_error").text("This email is already signed up to the newsletter!");
                        }
                    }
                    else if(response.status == "subscribed")
                    {
                        $("#newsletter_response").removeClass("is_error").addClass("is_success").text(email + " was successfully subscribed to our newsletter!");
                        $("#form__newsletter input[type=email]").val("");
                        $("#signup-form-container").hide();
                        $("#donate-container").show();
                    }
                }
            });
        });

        if($(".stripe").length)
        {
            var card = elements.create("card");
            card.mount("#card-element");

            $("#card-donate-submit:not([disabled])").on("click", function(e){
                e.preventDefault();
                $(this).attr("disabled");

                var amount = $("#donation-amount").val().replace(/[^a-zA-Z0-9 ]/g, '');
                var email = $("#donation-email").val();

                $.ajax({
                    url: '/p/secret',
                    data : {"donation-amount" : amount, "donation-email": email},
                    method: 'POST',
                    success: function(data) {
                        var clientSecret = data;

                        stripe.confirmCardPayment(clientSecret, {
                            payment_method: {
                                card: card,
                                billing_details: {
                                    email : email
                                }
                            }
                        }).then(function(result) {
                            if (result.error) {
                                // Show error to your customer (e.g., insufficient funds)
                                console.log(result.error.message);
                            }
                            else {
                                // The payment has been processed!
                                if (result.paymentIntent.status === 'succeeded') {
                                    $.ajax({
                                        url : '/p/confirm-donation',
                                        data : {"email" : email, "amount": $("#donation-amount").val()},
                                        method : 'POST',
                                        success : function(data){
                                            if(data == "success")
                                            {
                                                card.clear();
                                                $("#donation-amount").val("$0.00");
                                                $("#donation-email").val("");
                                                $("#donate-message").text("Thank you for donating! We greatly appreciate it!");
                                            }
                                        }
                                    })
                                }
                            }
                        });
                    }
                });
            });
        }

        if($(".subnav").length)
        {
            if (typeof($(".subnav").attr('pos') ) == 'undefined' || $(".subnav").attr("pos") == false)
            {
                $(".subnav").attr("pos", $(".subnav").offset().top)
            }
        }

        $("[anchor]").click(function(e){
            var goto = $(this).attr("anchor");

            var pos = $(goto).offset().top;

            if($(".subnav").length > 0)
            {
                pos = pos - $(".subnav").height();
            }

            $("html, body").animate({
                scrollTop : pos
            }, "400", "linear");

            if(!$(this).hasClass("no-hash"))
            {
                window.location.hash = goto.replace("#", "");
            }

            e.preventDefault();
        });

        if($(location).attr("hash"))
        {
            var hash = $(location).attr("hash");

            var pos = $(hash).offset().top;

            if($(".subnav").length > 0)
            {
                pos = pos - $(".subnav").height();
            }

            $("html, body").animate({
                scrollTop : pos
            }, "400", "linear");
        }
    });

    var scrollOriginal = 0;

    $(window).scroll(function(){
        var scrollTop = $(window).scrollTop();

        if($(".graph-homepage-container:not(.is__static)").length)
        {
            if(scrollTop + 300 > $(".graph-homepage-container:not(.is__static)").offset().top)
            {
                $(".graph-homepage-container:not(.is__static)").addClass("animate");
            }
        }

        if($(".subnav").length)
        {
            if (typeof($(".subnav").attr('pos') ) == 'undefined' || $(".subnav").attr("pos") == false)
            {
                $(".subnav").attr("pos", $(".subnav").offset().top)
            }

            if(scrollTop >= $(".subnav").attr("pos"))
            {
                $("header").css({
                    "margin-bottom" : $(".subnav").height()
                })
                $(".subnav").addClass("is__sticky");
            }
            else if(scrollTop <= $(".subnav").attr("pos"))
            {
                $(".subnav").removeClass("is__sticky");
                $("header").removeAttr("style");
            }
        }

        if($("header.is__interactive").length && $("header:not(.is__shown)"))
        {
            if($(window).scrollTop() > scrollOriginal)
            {
                $("header").addClass("is__shown");
            }
        }
    });

    $(window).resize(function(){
        //Homepage graphics being adjusted
        $(".graph-homepage-container:not(.is__static) .stripe_text").width(($(".homepage-bar").width()*3) + (parseInt($(".homepage-bar").css('margin-right'))*2)  + 90);
        $(".graph-homepage-line").width(($(".homepage-bar").width()*3) + (parseInt($(".homepage-bar").css('margin-right'))*2)  + 98);
        $(".graph-homepage-container:not(.is__static) .first_percentage").width(($(".homepage-bar").width()*3) + (parseInt($(".homepage-bar").css('margin-right'))*2)  + 68);
        $(".second_percentage").width(($(".homepage-bar").width()*2) + (parseInt($(".homepage-bar").css('margin-right')))  + 68);
        $(".third_percentage").width(($(".homepage-bar").width())  + 68);
    });
})(jQuery, Drupal);
