(function ($, Drupal) {
'use strict';

var stripe_publishable_key = "pk_test_rSwW0pLrqRWvhdfrAsZFlTUC00dTc47569";

$(window).on("load", function(){
    $("[data-type=currency]").inputmask(
        'decimal',
            { 'alias': 'numeric',
                'groupSeparator': '.',
                'autoGroup': true,
                'digits': 2,
                'radixPoint': ".",
                'digitsOptional': false,
                'allowMinus': false,
                'prefix': '$',
                'placeholder': '0',
                'rightAlign': false
            }
    ).on("blur", function(){
        if($(this).data("minimum"))
        {
            var min = $(this).data("minimum") + "";

            var num = $(this).val().replace(/[^0-9\.]/g, "");

            var array = num.split(".");

            if(array[0] < min)
            {
                $(this).val(min+".00");
            }
        }
    });

    $('#subscription-modal').on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget); // Button that triggered the modal
        var info = button.data('type'); // Extract info from data-* attributes
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this);

        modal.find(info).addClass("d-block");
        // modal.find('.modal-title').text('New message to ' + recipient);
        // modal.find('.modal-body input').val(recipient);
    }).on('hidden.bs.modal', function (event){
        var modal = $(this);

        modal.find(".d-block").removeClass("d-block");
    });

    $("#change-settings-modal").on('show.bs.modal', function(event){
        var button = $(event.relatedTarget);
        var info = button.data('type');

        var modal = $(this);

        modal.find(info).addClass("d-block");

    }).on('hidden.bs.modal', function (event){
        var modal = $(this);
        modal.find("input").val("");
        modal.find(".d-block").removeClass("d-block");
    });

    if($("#stripe-element"))
    {
        var stripe = Stripe(stripe_publishable_key);
        var elements = stripe.elements();

        var card = elements.create("card");
        card.mount("#stripe-element");

        card.on('change', function(event) {
          var displayError = document.getElementById('card-errors');
          if (event.error) {
            displayError.textContent = event.error.message;
          } else {
            displayError.textContent = '';
          }
        });

        $("#add-new-card").on("hidden.bs.modal", function(){
            var modal = $(this);

            card.clear();
            modal.find("input[type=checkbox]").prop('checked', false);
        });
    }

});
})(jQuery, Drupal);
