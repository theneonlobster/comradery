const { series, watch, dest, src } = require('gulp');
const sass = require('gulp-sass');
const nano = require('gulp-cssnano');

sass.compiler = require('node-sass');

exports.default = function() {
  watch('./sass/**/*.scss', function(){
      return src('./sass/**/*.scss')
          .pipe(sass.sync().on('error', sass.logError))
          .pipe(nano())
          .pipe(dest('./css'));
  });
};
