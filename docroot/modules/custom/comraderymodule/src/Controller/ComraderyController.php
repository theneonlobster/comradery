<?php

namespace Drupal\comraderymodule\Controller;

use Drupal\Core\Controller\ControllerBase;

class ComraderyController extends ControllerBase {

    public function payment(){
        return [
          '#markup' => 'Hello, world',
        ];
    }

    public function howcomraderyworks(){
        return [
          '#markup' => 'Hello, world',
        ];
    }

    public function whocomraderyisfor(){
        return [
          '#markup' => 'Hello, world',
        ];
    }


    public function presskit(){
        return [
          '#markup' => 'Hello, world',
        ];
    }

    public function signin(){
        if(\Drupal::currentUser()->isAuthenticated())
        {
            return $this->redirect('<front>');
        }
        else
        {
            return [
              '#markup' => 'Log into your account',
            ];
        }
    }

    public function signup(){
        return [
            '#markup' => 'Sign up'
        ];
    }

    public function subscriptions(){
        return [
            '#markup' => 'Your subscriptions'
        ];
    }

    public function subscribe(){
        return [
            '#markup' => 'Finish that sub, yo'
        ];
    }

    public function settings(){
        return [
            '#markup' => 'Account settings'
        ];
    }

    public function project(){
        return [
            '#markup' => 'Project'
        ];
    }

    public function projecttiers(){
        return [
            '#markup' => 'Project tiers'
        ];
    }
}

?>
